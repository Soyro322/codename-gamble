﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BetScript : MonoBehaviour
{
    public int bet;
    public float payout;
    public float funds;
    public string betOn;
    public Dropdown playerDD;
    public Dropdown bankerDD;
    public Dropdown tieDD;
    public Text fundText;

    public int[] betValues;

    private void Start()
    {
        //setting bet array
        betValues = new int[6] { 0, 5, 10, 25, 50, 100 };
        funds = 1000;
    }

    private void FixedUpdate()
    {
        fundText.text = "Funds: " + funds;
    }

    public void BetOnPlayer()
    {
        //set bet to player
        bet = betValues[playerDD.value];

        //reset other Dropdowns
        bankerDD.value = 0;
        tieDD.value = 0;

        //set what is betted on
        betOn = "Player";
    }

    public void BetOnBanker()
    {
        //set bet to banker
        bet = betValues[bankerDD.value];

        //reset other Dropdowns
        playerDD.value = 0;
        tieDD.value = 0;

        //set what is betted on
        betOn = "Banker";
    }

    public void BetOnTie()
    {
        //set bet to tie
        bet = betValues[tieDD.value];

        //reset other Dropdowns
        bankerDD.value = 0;
        playerDD.value = 0;

        betOn = "Tie";
    }
}
