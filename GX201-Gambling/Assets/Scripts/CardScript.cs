﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardScript : MonoBehaviour
{
    public CardScript cardScript;
    public string cardValue;
    public GameObject[] cards;
    public bool assigner;

    private void Start()
    {
        AssignCardScript();
        cardValue = gameObject.name.Substring(gameObject.name.Length - 2);
    }

    public void AssignCardScript()
    {
        if (assigner)
        {

            cardScript = this;

            cards = GameObject.FindGameObjectsWithTag("Card");

            foreach (var _card in cards)
            {
                _card.AddComponent<CardScript>();
            }

        }
    }
}
