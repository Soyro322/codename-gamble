﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class DeckScript : MonoBehaviour
{
    [Header("Arrays")]
    public int[] intArray;
    public int[] iDefaultDeck;
    public int[] iShuffledDeck;
    public int max;
    public Transform defaultDeckObject;
    public Transform shuffledDeckObject;
    //public List<Transform> shuffledList;
    public Transform[] defaultDeck;
    public Transform[] shuffledDeck;

    public Transform drawnCard;
    public CardScript drawnCardScript;
    public int drawnCardValue;
    public int bankerValue;
    public int playerValue;

    public Transform playerCard1;
    public Transform playerCard2;
    public Transform playerCard3;
    public Transform bankerCard1;
    public Transform bankerCard2;
    public Transform bankerCard3;
    public Text playerText;
    public Text bankerText;
    public Text winnerText;

    //public GameObject betScriptObject;
    public BetScript betScript;

    private void Start()
    {
        ResetDeck();
        ShuffleDeck();

        //betScript = betScriptObject.GetComponent<BetScript>();
    }

    public void DrawCard()
    {
        drawnCard = shuffledDeckObject.GetChild(0);
        drawnCard.localEulerAngles = Vector3.zero;
        
        drawnCardScript = drawnCard.gameObject.GetComponent<CardScript>();
        drawnCardValue = int.Parse(drawnCardScript.cardValue);
    }

    public void DealCards()
    {
        NextRound();

        betScript.funds += -betScript.bet;

        GetPlayerCards();
        GetBankerCards();

        playerText.text = "" + playerValue;
        bankerText.text = "" + bankerValue;

        BetPayouts();
    }

    public void ResetAll()
    {
        NextRound();
        betScript.funds = 1000;

        betScript.bankerDD.value = 0;
        betScript.tieDD.value = 0;
        betScript.playerDD.value = 0;

        betScript.bet = 0;
        betScript.betOn = "";
    }


    public void GetPlayerCards()
    {
        playerValue = 0;
        DrawCard();
        playerValue += drawnCardValue;
        drawnCard.SetParent(playerCard1);
        drawnCard.localPosition = Vector3.zero;
        DrawCard();
        playerValue += drawnCardValue;
        drawnCard.SetParent(playerCard2);
        drawnCard.localPosition = Vector3.zero;

        playerValue = playerValue%10;

        if (playerValue >=0 && playerValue <= 5)
        {
            DrawCard();
            playerValue += drawnCardValue;
            drawnCard.SetParent(playerCard3);
            drawnCard.localPosition = Vector3.zero;

            playerValue = playerValue % 10;
        }
    }

    public void GetBankerCards()
    {
        bankerValue = 0;
        DrawCard();
        bankerValue += drawnCardValue;
        drawnCard.SetParent(bankerCard1);
        drawnCard.localPosition = Vector3.zero;
        DrawCard();
        bankerValue += drawnCardValue;
        drawnCard.SetParent(bankerCard2);
        drawnCard.localPosition = Vector3.zero;

        bankerValue = bankerValue % 10;

        if (bankerValue >=0 && bankerValue <= 2)
        {
            DrawCard();
            bankerValue += drawnCardValue;
            drawnCard.SetParent(bankerCard3);
            drawnCard.localPosition = Vector3.zero;

            bankerValue = bankerValue % 10;
        }
    }
    
    public void ResetDeck()
    {
        max = 51;

        iDefaultDeck = new int[52];
        for (int k = 0; k < 52; k++)
        {
            iDefaultDeck[k] = k + 1;
        }

        defaultDeck = new Transform[52];
        for (int k = 0; k < 52; k++)
        {
            defaultDeck[k] = defaultDeckObject.transform.GetChild(k);
        }

        intArray = new int[52];
        for (int k = 0; k < 52; k++)
        {
            intArray[k] = k;
        }

        iShuffledDeck = new int[52];

        shuffledDeck = new Transform[52];
    }
    
    void ShuffleDeck()
    {
        int number;
        //int oldNumber;
        for (int k = 0; k < 52; k++)
        {
            int random = Random.Range(0, max);
            number = intArray[random];
            intArray[random] = intArray[max];
            intArray[max] = number;
            max--;
            
            shuffledDeck[k] = defaultDeck[number];
        }

        for (int k = 0; k < 52; k++)
        {
            shuffledDeck[k].SetParent(shuffledDeckObject);
        }
        
        /*for (int k = 0; k < 52; k++)
        {
            shuffledList.Add(shuffledDeck[k]);
        }
        */
    }

    public void NextRound()
    {
        //resets the deck       
        for (int k = 0; k < 52; k++)
        {
            defaultDeck[k].SetParent(defaultDeckObject);
            defaultDeck[k].localPosition = Vector3.zero;
            defaultDeck[k].localEulerAngles = new Vector3(180, 0, 0);
        }

        //shuffles again
        ResetDeck();
        ShuffleDeck();

        //resets labels
        playerText.text = "";
        bankerText.text = "";
        winnerText.text = "";
    }

    public void BetPayouts()
    {
        if (playerValue < bankerValue)
        {
            winnerText.text = "Banker Wins";
            if (betScript.betOn == "Banker")
                betScript.payout = betScript.bet * 2 * 9 / 10;
            else
                betScript.payout = 0;
        }
        else if (playerValue > bankerValue)
        {
            winnerText.text = "Player Wins";
            if (betScript.betOn == "Player")
                betScript.payout = betScript.bet * 2;
            else
                betScript.payout = 0;
        }
        else
        {
            winnerText.text = "Tie";
            if (betScript.betOn == "Banker")
                betScript.payout = betScript.bet * 9;
            else
                betScript.payout = 0;
        }

        betScript.funds += betScript.payout;
    }
}
